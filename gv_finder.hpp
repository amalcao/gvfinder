/***  gv_finder.hpp   ******************************************************
 *
 * This code is licensed under the new BSD license.
 * See the LICENSE.txt for details.
 *
 * Author: amalcaowei@gmail.com (Amal Cao)
 *
 *****************************************************************************/
#include <map>

#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/ADT/StringRef.h"

#include "clang/Lex/Preprocessor.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Rewrite/Core/Rewriter.h"

using namespace clang;

class GVFinderRecursiceASTVisitor
    : public RecursiveASTVisitor<GVFinderRecursiceASTVisitor> {
  
public:
  GVFinderRecursiceASTVisitor(std::string& FuncName)
    : SkipSearch_(!FuncName.empty()), FuncName_(FuncName) { }

  bool VisitDeclRefExpr(DeclRefExpr *E) {
    auto VD = dyn_cast<VarDecl>( E->getDecl() );

    if (!SkipSearch_ && VD && VD->hasGlobalStorage()) {
      auto VarName = VD->getNameAsString();
      assert(!CurFuncName_.empty() && "No name for current function!");

      Collection_[CurFuncName_].emplace_back(VarName);
    }
    return true;
  }

  bool VisitFunctionDecl(FunctionDecl *FD) {
    if (FD->hasBody()) {
      CurFuncName_ = FD->getDeclName() ? FD->getNameAsString() : "";

      if (SkipSearch_ && CurFuncName_ == FuncName_)
        SkipSearch_ = false;
    } 
    return true;
  }

  void printResult() {
    for (auto e : Collection_) {
      llvm::outs() << "<<< " << e.first << ">>> :\n";
      for (auto v : e.second) {
        llvm::outs() << "\t" << v << "\n";
      }
      llvm::outs() << "\n";
    }
  }

private:
  bool SkipSearch_;

  std::string &FuncName_;
  std::string CurFuncName_;
  std::map<std::string, std::vector<std::string> > Collection_;
};

class GVFinderASTConsumer : public ASTConsumer {
  GVFinderRecursiceASTVisitor Visitor_;
public:
  
  GVFinderASTConsumer(std::string& FuncName)
    : Visitor_(FuncName) {}

  bool HandleTopLevelDecl(DeclGroupRef D) override {
    
    for (auto beg = D.begin(), end = D.end();
         beg != end; ++beg) {
      Visitor_.TraverseDecl(*beg);
    }
    return true;
  }

  void printResult() {
    Visitor_.printResult();
  }
};

