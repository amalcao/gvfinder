/***  main.cpp   ******************************************************
 *
 * This code is licensed under the new BSD license.
 * See the LICENSE.txt for details.
 *
 * Author: amalcaowei@gmail.com (Amal Cao)
 *
 * The way using libclang to do the code rewritting is
 * referenced from the "Clang-tutorial" project.
 * The copyright of "Clang-tutorial" belongs to Larry Olson,
 * see the project's website
 * https://github.com/loarabia/Clang-tutorial for details.
 *
 *****************************************************************************/

#include <sys/types.h>
#include <system_error>

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/Parse/ParseAST.h"

#include "gv_finder.hpp"

using namespace clang;

/// FIXME : Auto generated system pre-defined macros,
///         got those by command line :
///         `clang-3.5 -E -dM - </dev/null`
static const char *sys_macros[] = {
#include "sys_macros.inc"
};

/// FIXME : Auto generated system include paths,
///         got those by command line :
///          `clang-3.5 -E -x c - -v < /dev/null`
static const char *sys_inc_path[] = {
#include "sys_inc_path.inc"
};

/// For comand line options
static llvm::cl::opt<std::string> InputFilename(llvm::cl::desc("<input file>"),
                                                llvm::cl::Required,
                                                llvm::cl::Positional);
/**
static llvm::cl::opt<std::string>
    OutputFilename("o", llvm::cl::desc("Output filename"),
                   llvm::cl::value_desc("filename"), llvm::cl::Optional);*/
static llvm::cl::list<std::string>
    IncludePaths("I", llvm::cl::value_desc("path"),
                 llvm::cl::desc("Add a directory to the header search path"),
                 llvm::cl::Prefix);

static llvm::cl::list<std::string>
    PredefMacros("D", llvm::cl::desc("Add a pre-defined macro"),
                 llvm::cl::Prefix);

static llvm::cl::opt<std::string>
    FunctionName("function", llvm::cl::value_desc("function name"),
                  llvm::cl::desc("The function to search"));



/// Main entry for mocc.
int main(int argc, char **argv) {
  // Before parsing the arguments, hidden some options defined by other
  // libraries.
  llvm::StringMap<llvm::cl::Option *> OptMap;
  llvm::cl::getRegisteredOptions(OptMap);

#define MAKE_ALIAS(AliasOp, AliasName, Opt)                                    \
  assert(OptMap.count(Opt) > 0);                                               \
  llvm::cl::alias AliasOp(AliasName, llvm::cl::desc("Alias for -" Opt),        \
                          llvm::cl::aliasopt(*OptMap[Opt]));

  // Make alias to pre-defined options
  MAKE_ALIAS(VersionAlias, "v", "version")
  MAKE_ALIAS(HelpAlias, "h", "help")

#undef MAKE_ALIAS

#define READLLY_HIDE(Opt)                                                      \
  if (OptMap.count(Opt) > 0) {                                                 \
    OptMap[Opt]->setHiddenFlag(llvm::cl::ReallyHidden);                        \
  }

  // FIXME : These options may introduced by libLLVMSupport.a
  READLLY_HIDE("stats")
  READLLY_HIDE("info-output-file")
  READLLY_HIDE("track-memory")
  READLLY_HIDE("view-background")
  READLLY_HIDE("debug")
  READLLY_HIDE("debug-buffer-size")
  READLLY_HIDE("debug-only")

  // FIXME : These options may introduced by libLLVMMC.a
  READLLY_HIDE("fatal-assembler-warnings")

#undef READLLY_HIDE

#if 0
  // Init the version print callback.
  // NOTE: It must be called before `ParseCommandLineOptions'!
  llvm::cl::SetVersionPrinter(
      []() { llvm::errs() << "mocc version \"" << GV_FINDER_VERSION << "\"\n"; });
#endif

  llvm::cl::ParseCommandLineOptions(argc, argv, "Global Variables Finder\n");

  // Create the compiler instance and invocation
  CompilerInstance Clang;

  Clang.createDiagnostics();

  // Set the diagnostic options
  Clang.getDiagnostics().getDiagnosticOptions().ShowColors = 1;

  llvm::SmallVector<const char *, 16> PPArgs;
  for (auto macro : PredefMacros) {
    PPArgs.push_back("-D");
    PPArgs.push_back(macro.c_str());
  }

  // Create an invocation that passes any flags to preprocessor
  CompilerInvocation *CI = new CompilerInvocation;
  CompilerInvocation::CreateFromArgs(
      *CI, const_cast<const char **>(PPArgs.data()),
      const_cast<const char **>(PPArgs.data()) + PPArgs.size(),
      Clang.getDiagnostics());

  Clang.setInvocation(CI);

  // Set default target triple
  std::shared_ptr<clang::TargetOptions> pto =
      std::make_shared<clang::TargetOptions>();
  pto->Triple = llvm::sys::getDefaultTargetTriple();
  TargetInfo *pti = TargetInfo::CreateTargetInfo(Clang.getDiagnostics(), pto);
  Clang.setTarget(pti);

  Clang.createFileManager();
  Clang.createSourceManager(Clang.getFileManager());

// Try to open the input file
#if defined(ENABLE_STDIN_AS_INPUT)
  FrontendInputFile Input(InputFilename, IK_C);
  if (!Clang.InitializeSourceManager(Input)) {
    exit(EXIT_FAILURE);
  }
#else
  const FileEntry *pFile = Clang.getFileManager().getFile(InputFilename);
  if (pFile == nullptr || !pFile->isValid()) {
    llvm::errs() << "failed to open the input file \"" << InputFilename
                 << "\"\n";
    exit(EXIT_FAILURE);
  }
  // Set the main file ID as the input file provided by CLI
  Clang.getSourceManager().setMainFileID(Clang.getSourceManager().createFileID(
      pFile, clang::SourceLocation(), clang::SrcMgr::C_User));
#endif

  // Initialize the pre-include paths
  // <Warning!!> -- Platform Specific Code lives here
  // This depends on A) that you're running linux and
  // B) that you have clang-3.5 installed that I do.

  HeaderSearchOptions &headerSearchOptions = Clang.getHeaderSearchOpts();

  for (auto path : sys_inc_path) {
    headerSearchOptions.AddPath(path, clang::frontend::System, false, false);
  }

  for (auto path : IncludePaths) {
    headerSearchOptions.AddPath(path, clang::frontend::Angled, false, false);
  }

  // Allow C99 code to get rewritten
  LangOptions langOpts;
  langOpts.GNUMode = 1;
  langOpts.Bool = 1;
  langOpts.C99 = 1;
  CI->setLangDefaults(langOpts, clang::IK_C, clang::LangStandard::lang_c99);

  // Create the pre-definitions
  Clang.createPreprocessor(clang::TU_Complete);
  Clang.getPreprocessorOpts().UsePredefines = true;

  Preprocessor &PP = Clang.getPreprocessor();

  std::string buf;
  llvm::raw_string_ostream predefines(buf);

  for (auto macro : sys_macros) {
    predefines << macro << "\n";
  }

  PP.setPredefines(predefines.str());

  Clang.createASTContext();

  // Initialize built-in info as long as we aren't using an external AST
  // source.
  if (Clang.hasASTContext() || !Clang.getASTContext().getExternalSource()) {
    PP.getBuiltinInfo().InitializeBuiltins(PP.getIdentifierTable(),
                                           PP.getLangOpts());
  }

  Clang.getDiagnosticClient().BeginSourceFile(Clang.getLangOpts(),
                                              &Clang.getPreprocessor());

  // Initialize rewriter
  Rewriter Rewrite;
  Rewrite.setSourceMgr(Clang.getSourceManager(), Clang.getLangOpts());

#if 0
  // Get the current module' info
  if (FunctionName.empty()) {
    FunctionName = "*";
  }
#endif

  // Create the AST consumer
  GVFinderASTConsumer astConsumer(FunctionName);

  // Parse the AST
  ParseAST(PP, &astConsumer, Clang.getASTContext());

  if (Clang.getDiagnostics().hasErrorOccurred()) {
    llvm::errs() << "\nfile not generated since some errors found "
                 << "during the translation ..\n";
    exit(EXIT_FAILURE);
  }

  // Print the result message ..
  astConsumer.printResult();

  return 0;
}
