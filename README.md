# A global variables finder for C programs

The `gvFinder` is a tool to search all references of the global variables
in your C program files. You can specific a function for searching, or not,
it will search all functions defined in the input c file.

The purpose for this naive tool is to show how to use the clang as libraries.

Before you build from source, you need:

- a pc running linux 
- llvm-3.5 & clang-3.5
- cmake 2.8+

--

This code is licensed under the new BSD license.
See the LICENSE.txt for details.
